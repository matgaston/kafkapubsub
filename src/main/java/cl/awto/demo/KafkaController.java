package cl.awto.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/kafka")
public class KafkaController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @GetMapping("/producer/{message}")
    private String testProducer(@PathVariable("message") String message) {
        kafkaTemplate.send("test", message);
        return "msg send";
    }


    /*@KafkaListener(topics = "test", groupId = "group_id")
    private void testConsumer(String message) {
        System.out.println("Consumer ha recibido: " + message);
    }*/

}
