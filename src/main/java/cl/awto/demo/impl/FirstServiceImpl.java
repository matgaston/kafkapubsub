package cl.awto.demo.impl;

import cl.awto.demo.TestService;
import org.springframework.stereotype.Service;

@Service
public class FirstServiceImpl implements TestService {
    @Override
    public String testMethodOne(String a) {
        return "First: " + a;
    }

    @Override
    public Integer testMethodTwo(Integer b) {
        return null;
    }
}
