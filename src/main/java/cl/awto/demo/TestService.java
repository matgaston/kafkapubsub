package cl.awto.demo;

public interface TestService {

    String testMethodOne(String a);

    Integer testMethodTwo(Integer b);
}
